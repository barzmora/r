setwd("C:/Users/user/Desktop/לימודים/שנה ד/סמסטר ב/R/תרגיל בית 3")

read.csv('houseDB.csv')
house.raw<- read.csv('houseDB.csv')
str(house.raw)

house.prepared<- house.raw

house.prepared$zipcode <- as.factor(house.prepared$zipcode)
str(house.prepared)

house.prepared$yr_built <- as.factor(house.prepared$yr_built)
house.prepared$floors <- as.factor(house.prepared$floors)

year <- substr(house.prepared$date,1,4)
house.prepared$year<- as.factor(year)

install.packages('ggplot2')
library(ggplot2)

#the connection of num of floors and price
ggplot(house.prepared, aes(floors,price)) + geom_boxplot()



house.prepared$date<-NULL
house.prepared$lat<-NULL
house.prepared$long<-NULL
house.prepared$sqft_living15<-NULL
house.prepared$sqft_lot15<-NULL
house.prepared$sqft_basement<-NULL


install.packages('caTools')
library(caTools)

#spliting the data to training and testing data
filter <- sample.split(house.prepared$floors, SplitRatio = 0.7)
house.train <- subset(house.prepared, filter == TRUE)
house.test <- subset(house.prepared, filter == FALSE)

dim(house.prepared)
dim(house.test)
dim(house.train)

house_model <- lm(price~., house.train)
summary(house_model)

house_predicted.test <- predict(house_model, house.test)
house_predicted.train <- predict(house_model, house.train)

#mean of test
house_test_mean<- mean(house_predicted.test)


house_MSE.test <- mean((house.test$price-house_predicted.test)**2)
house_MSE.train <- mean((house.train$price-house_predicted.train)**2)


house_RMSE_test <- house_MSE.test**0.5
house_RMSE_train <- house_MSE.train**0.5
